#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright (C) 2019 by Claudio Luck <cluck@posteo.ch>

# This program is licensed under the terms of the MIT License.

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

from __future__ import (
        unicode_literals,
        print_function
    )

from datetime import datetime, timedelta
from email.utils import parsedate_tz, mktime_tz
from functools import partial
from optparse import OptionParser

import base64
import binascii
import codecs
import contextlib
import email
import errno
import ftplib
import glob
import hashlib
import os
import re
import shlex
import shutil
import socket
import ssl  # Python 2.7.9+, Python 3.2+
import stat
import sys
import tarfile
import tempfile
import time
import zipfile

PY3K = sys.version_info[0] >= 3

if PY3K:
    from urllib.parse import splittype as url_splittype
    import urllib.parse as urlparse
    from urllib.request import (
            HTTPHandler, HTTPSHandler, ProxyHandler,
            # HTTPCookieProcessor,
            FTPHandler,
            build_opener, install_opener, urlopen,
            ftpwrapper, addclosehook, addinfourl )
    from urllib.error import URLError
    from http.client import HTTPSConnection

else:
    import urlparse
    from urllib import ftpwrapper, addclosehook, addinfourl
    from urllib2 import (
            splittype as url_splittype, URLError,
            HTTPHandler, HTTPSHandler, ProxyHandler,
            FTPHandler,
            # HTTPCookieProcessor,
            build_opener, install_opener, urlopen )
    from httplib import HTTPSConnection

# Optional(!) third-party modules to still support Python 2.7
try:
    import lzma
except ImportError:
    if PY3K:
        raise
    print("Warning: no support for LZMA in Python 2, use Python 3 instead.")
    lzma = None


socket_getaddrinfo = socket.getaddrinfo

def getaddrinfo4(*args, **kwargs):
    responses = socket_getaddrinfo(*args, **kwargs)
    return [r for r in responses if r[0] == socket.AF_INET]

def getaddrinfo6(*args, **kwargs):
    responses = socket_getaddrinfo(*args, **kwargs)
    return [r for r in responses if r[0] == socket.AF_INET6]

ssl_context, https_handler, url_opener = None, None, None   # initialized in main()
ssl_sock = None


class SpkHTTPSHandler(HTTPSHandler, object):

    def https_open(self, req):

        class SpkgHTTPSConnection(HTTPSConnection):

            def connect(self):
                global ssl_sock
                ret = HTTPSConnection.connect(self)
                # pc_der_txt = self.sock.getpeercert()
                # i_cn, i_c, i_o = None, None, None
                # for tok in pc_der_txt.get('issuer', ()):
                #     for prop, val in tok:
                #         if prop == 'countryName': i_c = val
                #         elif prop == 'organizationName': i_o = val
                #         elif prop == 'commonName': i_cn = val
                # print("# Issuer: CN=%s, O=%s, C=%s" % (i_cn, i_o, i_c))
                # pc_der = self.sock.getpeercert(binary_form=True)
                # pc_pem = ssl.DER_cert_to_PEM_cert(pc_der)
                # print(pc_pem)
                # ssl_sock = self.sock
                return ret

        return self.do_open(SpkgHTTPSConnection, req)


class SpkFTPWrapper(ftpwrapper, object):

    def retrfile(self, file, type_):
        try:
            lastmod = self.ftp.sendcmd('MDTM ' + file).split(None, 1)[1]
        except ftplib.error_perm:
            self.lastmod = lastmod = None
        if lastmod:
            lastmod = datetime.strptime(lastmod, "%Y%m%d%H%M%S")
            self.lastmod = lastmod.strftime("%a, %d %b %Y %H:%M:%S GMT")
        return ftpwrapper.retrfile(self, file, type_)


class FTPGetLastModifiedHandler(FTPHandler, object):

    def __init__(self, *args, **kw):
        self.debuglevel = kw.pop('debuglevel', 0)
        try:
            _super = FTPHandler.__init__
        except AttributeError:
            return
        _super(self, *args, **kw)

    def connect_ftp(self, *args, **kw):
        self.spk_fw = fw = SpkFTPWrapper(*args, **kw)
        if self.debuglevel >= 1:
            fw.ftp.set_debuglevel(1)
        return fw

    def ftp_open(self, req):
        aiu = FTPHandler.ftp_open(self, req)
        if self.spk_fw.lastmod:
            sf = "%s: %s\n%s" % ('Last-Modified', self.spk_fw.lastmod, aiu.headers)
            headers = email.message_from_string(sf)
            aiu2 = addinfourl(aiu, headers, aiu.url)
            return aiu2
        return aiu


class Package(object):

    # ORDER MATTERS!
    # - Keep from best/most-preferred to worst/least-preferred
    digests = ('sha512', 'sha384', 'sha256', 'sha224', 'sha1', 'md5')

    packages = {}

    def __init__(self, name, ver, ext):
        assert name
        F = '{0}({1}{2})'
        _xt = ext.strip('.')
        uniq_ctr, uniq_name = 2, name
        names = [p._uniq_name for p in Package.packages.get(name, ())]
        for f in (name, _xt, ''), (name, ver, ''), (name, ver, ext):
            uniq_name = F.format(*f)
            if uniq_name not in names:
                break
        while uniq_name in names:
            uniq_name = F.format(name, uniq_ctr, '')
        self._name = name
        self._uniq_name = uniq_name
        self.filename = None
        self.version = None
        self.fileext = None
        self._url = None
        self._checksums = {}
        self.tmpfile = None
        self.destfile = None
        self.check_result = None
        self.tags = set()
        Package.packages.setdefault(self._name, []).append(self)

    def __repr__(self):
        return "<Pkg %s>" % self._uniq_name

    @property
    def name(self):
        if len(Package.packages[self._name]) > 1:
            return self._uniq_name
        return self._name

    @property
    def url(self):
        return self._url

    @url.setter
    def url(self, url):
        self._url = urlparse.urlparse(url)

    @property
    def checksum(self):
        for ct in Package.digests:
            try:
                return (ct, self._checksums[ct])
            except KeyError:
                pass

    def set_checksum(self, value, checktype=None):
        """
        md5:     32 457484d2817fbe475ab582bff2014e82
        sha1:    40 242076dffbd432062b439335438f08ba53387897
        sha224:  56 89c0439b1cf3ec7489364a4b8e50b3ba196706eecdb5e5aec6d6290f
        sha256:  64 e10938435e4b5b54c9276c05d5f5d7c4401997fbd7f27f4d4...807d
        sha384:  96 3fe7c7bf3e83d70dba7d59c3b79f619cf821a798040be2177...edb7
        sha512: 128 fe50d9f0c5780edb8a8a41e317a6936ec6305d856c78ccb8e...1fa0
        """
        if not checktype:
            if ':' in value:
                ct, v = value.lower().split(':', 1)
                if ct not in Package.digests:
                    raise ValueError('Hash type: %s' %  ct)
                self._checksums[ct] = v
            else:
                ct = {  32: 'md5',  40: 'sha1',  56: 'sha224',
                        64: 'sha256',  96: 'sha384', 128: 'sha512'
                    }[len(value)]
                # assert ct in Package.digests
                self._checksums[ct] = value
        else:
            if checktype not in Package.digests:
                raise ValueError('Hash type: %s' %  ct)
            self._checksums[checktype] = value

    checksum = checksum.setter(set_checksum)

    def check_checksum(self, opts, filename, checktype, checksum):
        if checktype == 'md5':
            d = hashlib.md5()
        elif checktype == 'sha1':
            d = hashlib.sha1()
        elif checktype == 'sha224':
            d = hashlib.sha224()
        elif checktype == 'sha256':
            d = hashlib.sha256()
        elif checktype == 'sha384':
            d = hashlib.sha384()
        elif checktype == 'sha512':
            d = hashlib.sha512()
        else:
            raise ValueError('Hash type: %s' %  checktype)
        with open(filename, mode='rb') as f:
            for buf in iter(partial(f.read, 128), b''):
                d.update(buf)
        filesum = d.hexdigest()
        if checksum == filesum:
            return (checktype, filesum)
        return None

    def ignore_package(self, opt_tags, opt_notags):
        pos = set(opt_tags) & self.tags == set(opt_tags)
        neg = set(opt_notags) & self.tags
        return not (bool(pos) and not bool(neg) )

 


# Basically a copy of python3.7 urllib/request.py:urlretrieve
# Modified to support SSL context even on Python 2.7,
# and copied here because the original is marked *deprecated*.
#
def _urlretrieve(binurl, tmpfile, reporthook=None, context=None, **kw):
    url_type, path = url_splittype(binurl)

    with contextlib.closing(urlopen(binurl, None, context=None, **kw)) as fp:
        headers = fp.info()

        # Just return the local path and the "headers" for file://
        # URLs. No sense in performing a copy unless requested.
        if url_type == "file" and not tmpfile:
            return os.path.normpath(path), headers

        with open(tmpfile, 'wb') as fh:
            result = tmpfile, headers
            bs = 1024*8
            size = -1
            read = 0
            blocknum = 0
            if "content-length" in headers:
                size = int(headers["Content-Length"])

            if reporthook:
                reporthook(blocknum, bs, size)

            while True:
                block = fp.read(bs)
                if not block:
                    break
                read += len(block)
                fh.write(block)
                blocknum += 1
                if reporthook:
                    reporthook(blocknum, bs, size)

        if size >= 0 and read < size:
            raise ContentTooShortError(
                "retrieval incomplete: got only %i out of %i bytes"
                % (read, size), result)

        return result


def download(opts, urlobj, outpath, force=False):
    if not force and os.path.exists(outpath):
        # if opts.verbose:
        #     print(os.path.basename(outpath), end=': already present\n')
        return None
    try:
        prefix = os.path.basename(outpath)
        dir_ = os.path.dirname(outpath)
        (fd, tmpfile) = tempfile.mkstemp(".part", prefix=prefix, dir=dir_)
        os.close(fd)
        os.unlink(tmpfile)
        url = urlobj.geturl()
        if PY3K:
            # Python 3 can not quote URL as needed
            binurl = list(urlparse.urlsplit(url))
            binurl[2] = urlparse.quote(binurl[2])
            binurl = urlparse.urlunsplit(binurl)
        else:
            binurl = url
        def report(count, buflen, filelen):
            if opts.verbose:
                if PY3K:
                    print('.', end='', flush=True)
                else:
                    print('.', end='') ; sys.stdout.flush()
        if opts.verbose:
            print(outpath, end=': ')
        (tmpfile, headers) = _urlretrieve(binurl, tmpfile, report, timeout=7)
        lastmod = None
        if 'last-modified' in headers:
            lastmod = headers['Last-Modified']
            lastmod = mktime_tz(parsedate_tz(lastmod))
            utcnow = (datetime.utcnow() - datetime(1970,1,1)).total_seconds()
            os.utime(tmpfile, (utcnow, lastmod))
        if opts.verbose:
            print('')
    except URLError as e:
        import platform
        if 'Darwin' == platform.platform():
            sys.stderr.write(("\n*** You may need to run "
                "`/Applications/Python\\ 3.6/Install\\ Certificates.command'\n"
                "    to resolve the following problem:.\n\n"))
        raise e
    return tmpfile


def base_filename(fpkg):
    b = os.path.basename( fpkg )
    F, X = b, ''
    xi = len(b)
    f = F.lower()
    ext = ('.tar', '.gz', '.bz2', '.xz', '.zip', '.tgz', '.tbz', '.txz', '.exe', '.cab', '.dmg', None)
    while '.' in f:
        for x in ext:
            if x is None:
                return F, X
            if f.endswith(x):
                F = F[:-len(x)]
                f = f[:-len(x)]
                xi -= len(x)
                X = b[xi:]
                break
    return F, X


tokmatch = re.compile('([._~\\-\\+]+)([^._~\\-\\+]*)$')
vermatch = re.compile('[0-9][a-zA-Z0-9]*$')
def split_pkg_ver(name):
    toks = []
    n = name
    n0 = None
    vi = -1
    ct = -1
    while n0 != n:
        ct += 1
        m = tokmatch.search(n)
        if not m:
            toks.insert(0, ('', n))
            n0, vi = n, vi + 1
            break
        t = (m.group(1), m.group(2))
        toks.insert(0, t)
        n0, n = n, n[0:len(n) - len(t[0]) - len(t[1])]
        V = vermatch.match(m.group(2))
        if not V:
            vi += 1
        else:
            vi = 0
    n, v = '', ''
    midsep = toks[vi][0]
    toks[vi] = ('', toks[vi][1])
    for sep, tok in toks[0:vi]:
        n += sep + tok        
    for sep, tok in toks[vi:]:
        v += sep + tok
    return n, v


def main():

    global ssl_context, https_handler, url_opener

    dfl_playground = os.path.join('src', 'playground')
    dfl_package = 'packages'

    optParser = OptionParser()
    optParser.add_option("-P", "--playground", dest="playground", metavar="DIR",
            help="Set the path to the playground (%s)" % dfl_playground,
            default=dfl_playground)
    optParser.add_option("-p", "--packages", dest="packages", metavar="DIR",
            help="Set the path to the package directory (%s)" % dfl_package,
            default=dfl_package)

    optParser.add_option("-c", "--config", dest="config_file", metavar="CONFIG",
            help="Instructions file to process (packages.current), use '-' for stdin",
            default='packages.current')
    optParser.add_option("-s", "--status", dest="status_file", metavar="STATUS",
            help="Use a status file for caching (default is no status file)",
            default=None)
    optParser.add_option("-N", "--no-download", action="store_false", dest="do_download",
            help="Do not download missing packages", default=True)
    optParser.add_option("-C", "--skip-check", action="store_false", dest="do_check",
            help="Skip verification of checksums (bad idea)",
            default=True)

    optParser.add_option("-x", "--extract", action="store_true", dest="do_extract",
            help="Extract packages to playground after checking", default=False)

    optParser.add_option("-V", "--versions", action="store_true", dest="do_dump_versions",
            help="Output package versions (best use with -N)")

    optParser.add_option("-4", action="store_true", dest="do_ipv4_only",
            help="Use IPv4 only")
    optParser.add_option("-6", action="store_true", dest="do_ipv6_only",
            help="Use IPv6 only")

    optParser.add_option("-v", "--verbose", action="store_true", dest="verbose",
            help="Be verbose",
            default=False)
    optParser.add_option("-D", "--debug", action="store_true", dest="debug",
            help="Some debug output",
            default=False)

    optParser.add_option("-t", "--tag", action="append", type="str", dest="tags",
            help="Filter by tag",
            default=[])

    (opts, args) = optParser.parse_args()

    if opts.do_ipv6_only and opts.do_ipv4_only:
        raise ValueError("cannot force both IPv4 only and IPv6 only")
    if opts.do_ipv6_only:
        socket.getaddrinfo = getaddrinfo6
    elif opts.do_ipv4_only:
        socket.getaddrinfo = getaddrinfo4

    dlvl = 1 if opts.debug else 0
    if PY3K:

        ssl_context = ssl.create_default_context(ssl.Purpose.SERVER_AUTH)
        https_handler = SpkHTTPSHandler(debuglevel=dlvl, context=ssl_context, check_hostname=True)
    else:
        ssl_context = ssl.create_default_context(ssl.Purpose.SERVER_AUTH)
        if ssl_context.protocol < ssl.PROTOCOL_TLSv1_2:
            ssl_context.protocol = ssl.PROTOCOL_TLSv1_2 
        https_handler = SpkHTTPSHandler(debuglevel=dlvl, context=ssl_context)

    url_opener = build_opener(
            HTTPHandler(debuglevel=dlvl),
            FTPGetLastModifiedHandler(debuglevel=dlvl),
            https_handler,
            # ProxyHandler(), # {'http': self.PROXY, 'https': self.PROXY}
            # HTTPCookieProcessor(CookieJar(...)),
        )
    install_opener(url_opener)

    opts.status_mtime = 0
    if opts.status_file:
        try:
            opts.status_mtime = os.path.getmtime(opts.status_file)
        except OSError:
            if os.path.exists(opts.status_file):
                raise

    status = {}
    if opts.status_file:
        try:
            with codecs.open(opts.status_file, 'rb', 'utf-8') as fh:
                for line in fh:
                    mtime, fsize, checktype, checksum, fpkg = line.split(' ', 5)
                    fpkg = fpkg.rstrip('\n')
                    status[fpkg] = (float(mtime), int(fsize), checktype, checksum)
        except (OSError, IOError) as e:
            if getattr(e, 'errno', 0) != errno.ENOENT:
                raise
    opts.status = status

    tags = []
    notags = []
    for tag in opts.tags:
        for t in tag.split(','):
            t = t.strip()
            if t[0] == '-' or t[-1] == '-':
                notags.append(t.strip('-'))
            else:
                tags.append(t)
    if not len(tags): # and len(notags):
        tags = ['all']
    elif len(tags) and not len(notags):
        notargs = ['all']
    opts.tags = set(tags)
    opts.notags = set(notags)

    try:
        os.makedirs(opts.packages)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise

    try:
        os.makedirs(opts.playground)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise

    good_packages = []
    ret = main_process(opts, args, good_packages)
    if ret:
        return ret

    if opts.do_dump_versions:
        main_versions(opts, args, good_packages)

    if opts.do_extract:
        extract_packages(opts, args, good_packages)



def mkdir_p(*paths):
    for pth in paths:
        try:
            os.makedirs(pth)
            # print("mkdir -p", pth)
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise


class ExtractStrategies(object):

    def __init__(self, opts, p):
        self.opts = opts
        self.p = p
        self._tmpdir = None
        self._tmpdirs = {}
        self._castor = 1

    def dir(self, *names, **kw):
        mk = kw.pop('mk', False)
        if self._tmpdir is None:
            self._tmpdir = t0 = tempfile.mkdtemp(dir=self.opts.playground,
                        prefix='._spkdl+', suffix=".tmp.d")
        try:
            t = self._tmpdirs[names]
            return t
        except KeyError:
            pass
        self._tmpdirs[names] = ret = os.path.join(self._tmpdir, *names)
        if mk and not os.path.isdir(ret):
            mkdir_p(ret)
        return ret

    @property
    def tmpdir(self):
        return self.dir(mk=True)

    @property
    def xdir(self):
        return self.dir('x', mk=True)

    @property
    def olddir(self):
        return self.dir('old')

    def del_tmpdir(self, f1=None, f2=None):
        if self._tmpdir:
            shutil.rmtree(self._tmpdir)
            self._tmpdir = None

    class tar_bomb_castor(object):
        def __init__(self, path, xdir, name, components=None):
            self.path = path
            self.xdir = xdir
            self.name = name
            if isinstance(components, (tuple, list)):
                self.components = components
            elif components is not None:
                self.components = ((components, ''))
            else:
                self.components = True
        def __enter__(self):
            # if not os.path.isdir(self.xdir):
            #     mkdir_p(self.xdir)
            return self
        def __exit__(self, ctx_e, *ctx_t):
            if ctx_e is None:
                if self.components is True:
                    components = os.listdir(self.xdir)
                if len(components) != 1:
                    components = [(c, c) for c in components]
                else:
                    # look one level deeper
                    single = components[0]
                    print(os.path.join(self.xdir, single))
                    components = os.listdir(os.path.join(self.xdir, single))
                    components = [(os.path.join(single, c), c) for c in components]
                if os.path.isdir(self.path):
                    self.olddir = os.path.join(self.xdir, 'old')
                    shutil.move(self.path, self.olddir)
                mkdir_p(self.path)
                for component in components:
                    srcfile = os.path.join(self.xdir, component[0])
                    dstfile = os.path.join(self.path, component[1])
                    dstdir = os.path.dirname(dstfile)
                    mkdir_p(dstdir)
                    shutil.move(srcfile, dstfile)
            shutil.rmtree(self.xdir)
            return ctx_e is None

    def castor(self, tgt):
        self._castor = c = self._castor + 1
        name = self.p._name + '-' + self.p.version
        return ExtractStrategies.tar_bomb_castor(tgt, self.dir('castor%d' % c, mk=True), name)

    def x_gz(self, f1, f2):
        print('x gz', f1, f2)
        import gzip
        with open(f2, 'wb') as fout:
            with gzip.open(f1, 'rb') as fin:
                for buf in iter(partial(fin.read, 128), b''):
                    fout.write(buf)

    def xd_tar(self, f1, f2):
        print('x tar', f1, f2)
        with self.castor(f2) as b:
            with tarfile.open(f1, 'r') as tarf:
                tarf.extractall(path=b.xdir)

    def xd_tar(self, f1, f2):
        print('x tar', f1, f2)
        with self.castor(f2) as b:
            with tarfile.open(f1, 'r') as tarf: 
                tarf.extractall(path=b.xdir)

    def xd_tar_gz(self, f1, f2):
        print('x tar.gz', f1, f2)
        with ExtractStrategies.tar_bomb_castor(f2, self.dir('castor')) as b:
            with tarfile.open(f1, 'r:gz') as tarf: 
                tarf.extractall(path=b.xdir)

    def xd_tar_bz2(self, f1, f2):
        print('x tar.bz2', f1, f2)
        with self.castor(f2) as b:
            with tarfile.open(f1, 'r:bz2') as tarf: 
                tarf.extractall(path=b.xdir)

    def xd_tar_xz(self, f1, f2):
        print('x tar.xz', f1, f2)
        if lzma is None:
            raise Exception('lzma module missing (PIP package pyliblzma)')
        with self.castor(f2) as b:
            with contextlib.closing(lzma.LZMAFile(f1, mode='r')) as xzf:
                with tarfile.open(mode='r', fileobj=xzf) as tarf:
                    tarf.extractall(path=b.xdir)

    def xd_zip(self, f1, f2):
        print('x zip', f1, f2)
        with self.castor(f2) as b:
            z = zipfile.ZipFile(f1, 'r')
            dirs, dirQ = [], []
            with z as zipf:
                # zipf.extractall(b.xdir) -- no support for os.utime()
                for zinfo in zipf.infolist():
                    tgt = tgtdir = os.path.join(b.xdir, zinfo.filename)
                    is_dir = zinfo.filename[-1] == '/'
                    filemode = (zinfo.external_attr >> 16) & 1535  # = 02777
                    date_time = time.mktime(zinfo.date_time + (0, 0, -1))
                    if not is_dir:
                        tgtdir = os.path.dirname(tgt)
                    if is_dir and tgtdir not in dirs:
                        dirs.append(tgtdir)
                        dirQ.append((date_time, filemode))
                    mkdir_p(tgtdir)
                    with open(tgt, 'wb') as tgtf:
                        with z.open(zinfo) as srcf:
                            for buf in iter(partial(srcf.read, 128), b''):
                                tgtf.write(buf)
                    os.chmod(tgt, filemode)
                    os.utime(tgt, (date_time, date_time))
            for i, tgtdir in enumerate(dirs):
                date_time, filemode = dirQ[i]
                os.chmod(tgtdir, filemode)
                # os.utime(tgtdir, (date_time, date_time))

    def x_exe(self, f1, f2):
        print('x exe', f1, f2)
        mkdir_p(f2)
        shutil.copy(f1, os.path.join(f2, self.p.filename))



def extract_packages(opts, args, good_packages):
    
    def _mv(f1, f2):
        print('x mv -r', f1, f2)
        if os.path.isdir(f2):
            shutil.rmtree(f2)
        elif os.path.exists(f2):
            os.unlink(f2)
        mkdir_p(os.path.dirname(f2))
        shutil.move(f1, f2)

    queue = []
    for p in good_packages:
        pkg_queue = []
        xs = ExtractStrategies(opts, p)
        ext_consumed = ''
        ext_remain = p.fileext.lstrip('.')
        ext = ext_remain.split('.')
        f1 = os.path.join(opts.packages, p.filename)
        f2 = None
        while len(ext):
            ext_tok_processed = False
            for i in range(0, len(ext), 1):  # ext = ('', 'dir1', 'dir2', ...)
                # [...], x_xt1_xt_xt3, x_xt1_xt2, x_xt1
                try:
                    try:
                        fx, dx, x = None, None, '_'.join(ext[i:])
                        dx = getattr(xs, 'xd_' + x)
                    except AttributeError:
                        fx = getattr(xs, 'x_' + x)
                except AttributeError:
                    continue
                ext_consumed = '.' + '.'.join(ext[i:])
                ext_remain = '.' + '.'.join(ext[0:i])
                f0 = f1
                if dx:
                    f1 = os.path.join(xs.dir(p.name + ext_consumed + '.d', mk=True))
                elif fx:
                    f1 = os.path.join(xs.dir(p.name + ext_consumed, mk=True), 'unpack' + ext_remain)
                # print('>', repr(ext_consumed), repr(ext_remain))
                pkg_queue.append((xs, bool(dx), dx or fx, f0, f1))
                ext_tok_processed = True
                break
            ext = ext[0:i]
            if not ext_tok_processed:
                break
                #raise Exception("don't know how to extract '%s'" % p.filename)
        if len(pkg_queue):
            # is_dir = pkg_queue[-1][1]
            f1 = pkg_queue[-1][4]
            f2 = os.path.join(opts.playground, 'build', p.name + '-' + p.version)
            pkg_queue.append((xs, None, _mv, f1, f2))
            pkg_queue.append((xs, False, xs.del_tmpdir, None, None))
        queue.extend(pkg_queue)
        instdir = os.path.join(opts.playground, 'install', p.name, p.version)
        mkdir_p(instdir)
    for xs, is_dir, fc, f1, f2 in queue:
        fc(f1, f2)




def main_versions(opts, args, good_packages):
    for p in good_packages:
        print("  package %s version=%s" % (p.name, p.version))


def main_process(opts, args, good_packages):

    parse_step = 1
    server, file_ = None, None
    p = None

    ctx = {
        'download_failed': False,
        'check_failed': False,
        'checks_passed': 0,
        'status': opts.status,
        'dfltags': set(('all',)),
        'srvtags': set(),
        'pkgtags': set(),
    }

    def set_head(ctx, verb, *args):
        if verb == 'dfltags':
            ctx['dfltags'] = set(args)
        else:
            ctx[verb] = args[0]

    def new_server(ctx, verb, *args):
        ctx['server'] = None
        ctx['srvtags'] = set()

    def set_server(ctx, verb, *args):
        if verb == 'srvtags':
            ctx['srvtags'] = set(args)
        else:
            ctx[verb] = args[0]

    def new_package(ctx, verb, *args):
        ctx['file'] = None
        ctx['name'] = None
        ctx['pkg'] = None
        ctx['ver'] = None
        ctx['pkgtags'] = set()
        ctx['p'] = None

    def set_package(ctx, verb, *args):
        # if verb == 'name':
        #     #verb = 'pkg'
        #     ctx['name'] = args[0]
        if verb == 'srvtags':
            set_server(ctx, verb, *args)
        elif verb == 'tags':
            ctx['pkgtags'] = set(args)
        elif verb == 'pkg':
            ctx['pkg'] = args[0]
            if len(args) > 1:
                ctx['ver'] = args[1]
        elif verb == 'ver':
            ctx['ver'] = args[0]
        else:
            ctx[verb] = args[0]

    def create_package(ctx, verb, *args):
        # ext/fileext is a hint for the unpacking strategy
        pkg, npkg, fpkg = ctx.get('pkg'), None, None
        ver, nver, fver = ctx.get('ver'), None, None
        ext, next_, fext = '', None, None
        if ctx['name']:
            nbase, next_ = base_filename(ctx['name'])
            npkg, nver = split_pkg_ver(nbase)
        pkg = pkg if pkg else npkg
        ver = ver if ver else nver
        ext = ext if ext else next_
        if ctx['file']:
            fbase, fext = base_filename(ctx['file'])
            fpkg, fver = split_pkg_ver(fbase)
        pkg = pkg if pkg else fpkg
        ver = ver if ver else fver
        ext = ext if ext else fext
        #
        ctx['p'] = p = Package(pkg, ver, ext)
        # ctx['pkg'] = p.name   # the name may have been mangled to make it unique
        p.url = ctx['server'] + '/' + ctx['file']
        p.version = ver
        p.filename = os.path.basename( ctx['name'] or ctx['file'] )
        p.fileext = ext
        p.tags = tags = ctx['dfltags'] | ctx['srvtags'] | ctx['pkgtags']
        ctx['p'] = p

    def set_check(ctx, verb, *args):
        p = ctx['p']
        p.checksum = args[0]

    def dl_file(ctx, verb, *args):
        p = ctx['p']
        if p.ignore_package(opts.tags, opts.notags):
            return
        u = p.url
        # f = os.path.basename(u.geturl())
        p.destfile = os.path.join(opts.packages, p.filename)
        if not opts.do_download:
            return
        # mitigate lenient caching proxies...
        # trying to follow Subresource Integrity (SHA384) scheme
        if u.scheme in ('http', 'https'):
            CHK = p._checksums
            chk = ('sha384', CHK['sha384']) if 'sha384' in CHK else tuple(p.checksum)
            h = base64.b64encode( binascii.unhexlify(chk[1]) ).decode('ascii')
            h = '-'.join((chk[0], h ))
            t = (datetime.utcnow() - datetime(1970,1,1)).total_seconds()
            q2 = 'integrity={0:s}&ts={1:.6f}'.format(h, t)
            if u.query:
                q2 = '&'.join((u.query, q2))
            u = u._replace(query=q2)
        try:
            p.tmpfile = download(opts, u, p.destfile, False)
        except Exception as e:
            ctx['download_failed'] = True
            print('ERROR: %s: %s' % (type(e).__name__, str(e)))
            raise
            #return

    def check_file(ctx, verb, *args):
        p = ctx['p']
        if p.ignore_package(opts.tags, opts.notags):
            return
        chkfile = p.tmpfile if p.tmpfile else p.destfile
        check_passed = None
        check_result = None
        if not opts.do_check:
            check_passed = 'skipped'
        else:
            try:
                cached_check = opts.status[p.filename]
            except KeyError:
                pass
            else:
                try:
                    fst = os.stat(p.destfile)
                except (IOError, OSError):
                    pass
                else:
                    if (cached_check[0] >= fst.st_mtime and cached_check[1] == fst.st_size):
                        if (fst.st_mtime <= opts.status_mtime):
                            if (cached_check[2] in p._checksums and cached_check[3] in p._checksums[cached_check[2]]):
                                check_passed = 'OK (cached)'

        # Note: check_checksum returns None or the checksum (string)
        if not check_passed:
            check_result = p.check_checksum(opts, chkfile, *p.checksum)
            check_passed = 'OK' if check_result else None
        if check_passed is None:
            ctx['check_failed'] = True
            print("%s: %s" % (p.filename, "FAILED"))
        else:
            if opts.do_check:
                print("%s: %s" % (p.filename, check_passed))
            ctx['checks_passed'] += 1
            good_packages.append( ctx['p'] )
            if p.tmpfile:
                shutil.move(p.tmpfile, p.destfile)
                p.tmpfile = None
        if check_result:
            fst = os.stat(p.destfile)
            opts.status[p.filename] = (fst.st_mtime, fst.st_size, check_result[0], check_result[1])
        if p.tmpfile:
            os.unlink(p.tmpfile)
            p.tmpfile = None

    hdr_verbs = ('dfltags', )
    srv_verbs = ('server', 'srvtags',)
    pkg_verbs = ('file', 'name', 'pkg', 'ver', 'tags', 'srvtags')
    parser = [
        # cur-state,   verbs,        actions,                          next-state
        (('head',),    hdr_verbs,    (set_head, ),                     'head'),
        (('head',),    srv_verbs,    (new_server, set_server, ),       'server'),
        (('server',),  srv_verbs,    (set_server, ),                   'server'),
        (('server',),  pkg_verbs,    (new_package, set_package),       'package'),
        (('package',), pkg_verbs,    (set_package,),                   'package'),
        (('package',), ('chk',),     (create_package, set_check,),     'check'),
        (('check',),   ('chk',),     (set_check,),                     'check'),
        (('check',),   srv_verbs,    (dl_file, check_file, new_package, new_server, set_server),
                                                                       'server'),
        (('check',),   pkg_verbs,    (dl_file, check_file, new_package, set_package),
                                                                       'package'),
        (('check',),   ('EOF',),     (set_check, dl_file, check_file), 'EOF'),
    ]
    state = 'head'

    def run(ctx, parser, state, cmd, args):
        for curstate, verbs, actions, nextstate in parser:
            if state in curstate and cmd in verbs:
                for action in actions:
                    # print(ctx.get('p'), state, cmd, action.__name__)
                    action(ctx, cmd, *args)
                state = nextstate
                return True, state
        return False, state

    with codecs.open(opts.config_file, 'rb', 'utf-8') as fh:
        linecount = 0
        for line in fh:
            linecount += 1
            do_check = False
            do_new_pkg = False
            line = line.lstrip()
            if not line or line.startswith("#"):
                continue
            try:
                cmd, args = line.split(None, 1)
                args = args.rstrip('\r\n')
                args = shlex.split(args)
                stm_ok, state = run(ctx, parser, state, cmd, args)
                if not stm_ok:
                    raise SyntaxError("invalid command '%s' in state '%s'" % (cmd, state))
            except Exception as e:
                print("%s before or at line %d: %s" % (type(e).__name__, linecount, line))
                import traceback
                traceback.print_exc()
                raise
        stm_ok, state = run(ctx, parser, state, 'EOF', args)
    if opts.status_file:
        with codecs.open(opts.status_file, 'wb', 'utf-8') as fh:
            for f in opts.status:
                fh.write('%.17g %s ' % (opts.status[f][0], opts.status[f][1]))
                fh.write('%s %s %s\n' % (opts.status[f][2], opts.status[f][3], f))

    if ctx['checks_passed'] == 0:
        return 1
    return 1 if ctx['check_failed'] else 0


if __name__ == "__main__":
    sys.exit(main())

