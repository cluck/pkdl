
PKMAN_VER           := 1.0.4
PKMAN_TEST_SERVER   := https://gitlab.com/cluck/pkdl/raw/$(PKMAN_VER)
PKMAN_TEST_PATH     := test/

PKMAN_REL_SERVER    := https://gitlab.com/cluck/pkdl/raw/$(PKMAN_VER)
PKMAN_REL_PATH      := release/

DATE_FMT = %Y-%m-%d
ifdef SOURCE_DATE_EPOCH
    BUILD_DATE ?= $(shell date -u -d "@$(SOURCE_DATE_EPOCH)" "+$(DATE_FMT)"  2>/dev/null || date -u -r "$(SOURCE_DATE_EPOCH)" "+$(DATE_FMT)" 2>/dev/null || date -u "+$(DATE_FMT)")
else
    BUILD_DATE ?= $(shell date "+$(DATE_FMT)")
endif

.PHONY: all build test live-test release clean

all: _all

# My personal "upload" recipe is not for everybody's eyes
-include local.mk

build/pkdl-$(PKMAN_VER)/spkdl.py:
	mkdir -p build/pkdl-$(PKMAN_VER)
	install -m 755 spkdl.py build/pkdl-$(PKMAN_VER)/spkdl.py

build/pkdl-$(PKMAN_VER).zip:  Makefile build/pkdl-$(PKMAN_VER)/spkdl.py
	rm -f build/pkdl-$(PKMAN_VER).zip
	find build ! -type d -print0 | xargs -0 -n1 touch -r spkdl.py
	find build -type d -print0 | xargs -0 -n1 touch -r spkdl.py
	@# zip: for stable checksums, no not add the directories themselves
	cd build ; zip -9 -D -vr pkdl-$(PKMAN_VER).zip  pkdl-$(PKMAN_VER)/spkdl.py
	# touch -r spkdl.py build/pkdl-$(PKMAN_VER).zip

build/packages.current:  build/pkdl-$(PKMAN_VER).zip
	echo "# Example packages.current" >$@
	echo "server $(PKMAN_TEST_SERVER)" >>$@
	echo "file $(PKMAN_TEST_PATH)pkdl-$(PKMAN_VER).zip" >>$@
	echo "chk $(shell sha256sum build/pkdl-$(PKMAN_VER).zip | cut -d' ' -f1)" >>$@
	echo >>$@

release/pkdl-$(PKMAN_VER).zip:  build/pkdl-$(PKMAN_VER).zip
	mkdir -p release
	install -m 0644 build/pkdl-$(PKMAN_VER).zip release/pkdl-$(PKMAN_VER).zip

packages.current:  release/pkdl-$(PKMAN_VER).zip
	echo "# Example PKDL release packages.current" >$@
	echo "server $(PKMAN_REL_SERVER)" >>$@
	echo "file $(PKMAN_REL_PATH)pkdl-$(PKMAN_VER).zip" >>$@
	echo "chk $(shell sha256sum release/pkdl-$(PKMAN_VER).zip | cut -d' ' -f1)" >>$@
	echo >>$@

release:  packages.current release/pkdl-$(PKMAN_VER).zip

clean:
	rm -rf build test

test:  build/packages.current
	mkdir -p test test/packages
	install build/packages.current test/packages.current
	install build/pkdl-$(PKMAN_VER)/spkdl.py test/spkdl.py
	install build/pkdl-$(PKMAN_VER).zip test/packages/pkdl-$(PKMAN_VER).zip
	cd test ; ./spkdl.py -N -v -x

live-test:  build/packages.current
	mkdir -p test test/packages
	install build/packages.current test/packages.current
	install build/pkdl-$(PKMAN_VER)/spkdl.py test/spkdl.py
	rm -f test/packages/pkdl-$(PKMAN_VER).zip
	cd test ; ./spkdl.py -v -x

_all:  build/pkdl-$(PKMAN_VER).zip build/packages.current
